// Logic to check if the element is in the view port
$.fn.isInViewport = function () {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();
	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();
	return elementBottom > viewportTop && elementTop < viewportBottom;
};
//call the isInViewport function to check if the following element is in the viewport

//The following is for selecting many elements with classes
$(window).on('resize scroll', function () {
	//Get refrence to the section
	var selected_boxes = $('.box1');
	// for each element that matches
	selected_boxes.each(function (i, el) {
		// add jquery selector for el => refrence to each element that matched
		var box3 = $(el);
		//Checks if the element is in viewport
		if (box3.isInViewport()) {
			box3.addClass('fadeInLeftBig');
		} else {
			box3.removeClass('fadeInLeftBig');
		}
	});
});

// check if element is already in viewport and add delay or remove class accordingly

var selected_boxes = $('.box1');

selected_boxes.each(function (i, el) {
	// add jquery selector for el => refrence to each element that matched
	var box3 = $(el);
	//Checks if the element is in viewport
	if (box3.isInViewport()) {
		setTimeout(function(){
			box3.addClass('fadeInLeftBig');
		}, 900);
		
	}
})